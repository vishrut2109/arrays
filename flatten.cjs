let result = [];
function flatten(elements, depth = 1) {
  if (!Array.isArray(elements) || elements.length == 0) {
    return [];
  }

  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index])) {
      if (depth >= 1) {
        flatten(elements[index], depth - 1);
      } else {
        result.push(elements[index]);
      }
    } else {
      if (elements[index] != undefined) {
        result.push(elements[index]);
      }
    }
  }
  return result;
}

module.exports = flatten;
