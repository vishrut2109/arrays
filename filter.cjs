function filter(elements, callBac) {
  let result = [];
  if (elements && Array.isArray(elements)) {
    for (let index = 0; index < elements.length; index++) {
      if (callBac(elements[index], index, elements) === true) {
        result.push(elements[index]);
      }
    }

    return result;
  }
}

module.exports = filter;
