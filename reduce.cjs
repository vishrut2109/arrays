function reduce(elements, callBac, startingValue) {
  let result = [];
  let index1 = 0;
  let sum = 0;
  if (elements && Array.isArray(elements)) {
    if (startingValue === undefined) {
      startingValue = elements[0];
      index1 = 1;
    }
    for (let index = index1; index < elements.length; index++) {
      sum = callBac(startingValue, elements[index], index, elements);
      startingValue = sum;
    }
    return sum;
  }
}

module.exports = reduce;
