const arr = [1, 2, 3, 4, 5];
const filter = require("../filter.cjs");

function callBac(value, index, elements) {
  return elements[index] % 2 === 1;
}

const result = filter(arr, callBac);
console.log(result);
