const arr = [1, 2, 3, 4, 5];
const find = require("../find.cjs");

function callBac(value, index) {
  return value === 5;
}

const result = find(arr, callBac);
console.log(result);
