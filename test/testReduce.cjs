const arr = [1, 2, 3, 4, 5];
const startingValue = 0;
const reduce = require("../reduce.cjs");

function callBac(startingValue, value, index, elements) {
  return startingValue + value;
}

const result = reduce(arr, callBac, startingValue);
console.log(result);
