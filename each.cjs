function each(elements, callBac) {
  // based off http://underscorejs.org/#each

  if (elements && Array.isArray(elements)) {
    for (let index = 0; index < elements.length; index++) {
      callBac(elements[index]);
    }
  }
}

module.exports = each;
