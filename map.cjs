function map(elements, callBac) {
  let result = [];
  if (elements && Array.isArray(elements)) {
    for (let index = 0; index < elements.length; index++) {
      result.push(callBac(elements[index], index, elements));
    }
    return result;
  }
}

module.exports = map;
