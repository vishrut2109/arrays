function find(elements, callBac) {
  if (elements && Array.isArray(elements)) {
    for (let index = 0; index < elements.length; index++) {
      if (callBac(elements[index], index)) {
        return elements[index];
      }
    }
    return undefined;
  }
}
module.exports = find;
